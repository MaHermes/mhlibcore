using System;
using UnityEngine;

namespace MHLib.Runtime
{
    public interface IManagedStateStore
    {
        void Store(string _storeKey, string _value);
        string Get(string _storeKey, string _defaultValue = "");
    }

    public enum EStateUpdateContext
    {
        Default, // a regular request from outside
        Initial, // from initializing the managed state
        ListenerRegistered,
        Dirty
    }

    public interface IManagedState { }

    /// <summary>
    /// manages a struct (the state). updates, notifies about and persists (PlayerPrefs) it optionally
    /// </summary>
    public class CManagedState<S> : IManagedState, IManagedStateStore where S : struct
    {
        public Action<S, S> OnStateChanged;
        public Action<S, S, EStateUpdateContext> OnStateChangedWithContext;

        public S State { get; private set; }

        private Func<S, S, EStateUpdateContext, S> m_updateValidator; // prev state, next state, returns validated state
        private string m_storeKey;
        private IManagedStateStore m_store;

        /// <summary>
        /// initializes state management
        /// </summary>
        /// <param name="_storeKey">key used for player prefs. leave empty for no persistence</param>
        /// <param name="_initialState">state to set initially</param>
        /// <param name="_updateValidator">called on state update request before state is updated. 
        /// receives current and requested state and can optionally apply changes before state is changed</param>
        public void Initialize(string _storeKey = "", S _initialState = default, Func<S, S, EStateUpdateContext, S> _updateValidator = null, IManagedStateStore _store = null)
        {
            m_updateValidator = _updateValidator;
            m_storeKey = _storeKey;
            m_store = _store ?? this; // is store overriden set? use it, otherwise we handle it our own with player prefs

            var nextState = _initialState;

            if (!string.IsNullOrEmpty(_storeKey))
                nextState = JsonUtility.FromJson<S>(m_store.Get(m_storeKey, JsonUtility.ToJson(nextState)));

            RequestUpdate(_state =>
            {
                return nextState;
            }, EStateUpdateContext.Initial);
        }

        public void RegisterAndNotifyListener(Action<S, S> _listener)
        {
            OnStateChanged -= _listener;
            OnStateChanged += _listener;
            _listener.Invoke(State, State);
        }

        public void RegisterAndNotifyListener(Action<S, S, EStateUpdateContext> _listener)
        {
            OnStateChangedWithContext -= _listener;
            OnStateChangedWithContext += _listener;
            _listener.Invoke(State, State, EStateUpdateContext.ListenerRegistered);
        }

        public void UnRegisterListener(Action<S, S> _listener) => OnStateChanged -= _listener;
        public void UnRegisterListener(Action<S, S, EStateUpdateContext> _listener) => OnStateChangedWithContext -= _listener;

        public void RequestUpdate(Func<S, S> _updater, EStateUpdateContext _context = EStateUpdateContext.Default, bool _instantFlush = true)
        {
            var prevState = State;
            var nextState = _updater(State);

            if (m_updateValidator != null)
                nextState = m_updateValidator(State, nextState, _context);

            State = nextState;
            OnStateChanged?.Invoke(prevState, State);
            OnStateChangedWithContext?.Invoke(prevState, State, _context);
            if (_instantFlush)
                FlushState();
        }

        public void FlushState()
        {
            if (!string.IsNullOrEmpty(m_storeKey))
                m_store.Store(m_storeKey, JsonUtility.ToJson(State));
        }

        // IManagedStateStore interface implementation
        public void Store(string _storeKey, string _content) => PlayerPrefs.SetString(m_storeKey, _content);
        public string Get(string _storeKey, string _default = "") => PlayerPrefs.GetString(m_storeKey, _default);
    }
}
