using System;

namespace MHLib.Runtime
{
    [Flags]
    public enum EExamplesCheatAction : long
    {
        None = 0,
        CheatEndBattle = (1 << 0),
        PlaceHolder = (1 << 1),
    }

    public class CEventBusCheatAction : CEventBus<EExamplesCheatAction>
	{
        public class CEndBattleParams : CEventBusParams 
        { 
            public bool Test;
            public CEndBattleParams(bool prm) { Test = prm; }
        }
    }
}
