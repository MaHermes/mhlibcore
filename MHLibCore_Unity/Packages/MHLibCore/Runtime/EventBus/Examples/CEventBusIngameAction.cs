using System;

namespace MHLib.Runtime
{
    [Flags]
    public enum EExampleIngameAction : int
    {
        None = 0,
        RequestPause = (1 << 0),
        PlaceHolder = (1 << 1),
    }

    public class CEventBusIngameAction : CEventBus<EExampleIngameAction>
	{
    }
}
