﻿using System;
using UnityEngine;

namespace MHLib.Runtime
{
    public abstract class CEventBusComponent<T> : MonoBehaviour, IEventBusListener<T> where T : Enum
    {
        public abstract T GetEventsOfInterest();
        public abstract void OnBusEvent(T _event, CEventBusParams _params);

        protected virtual void Awake()
        {
            this.RegisterToEventBus();
        }

        protected virtual void OnDestroy()
        {
            this.UnregisterFromEventBus();
        }
    }
}
