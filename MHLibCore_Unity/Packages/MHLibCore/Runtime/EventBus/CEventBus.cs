﻿using System;
using System.Collections.Generic;

namespace MHLib.Runtime
{
    public static class EventBusExtensions
    {
        public static T As<T>(this CEventBusParams _prm) where T : CEventBusParams => _prm as T;

        public static IEventBusListener<T> Register<T>(this T _eventFlags, Action<CEventBusParams> _action, bool _once) where T : Enum
        {
            return CEventBus<T>.Instance.RegisterListener(_eventFlags, _action, _once);
        }

        public static void Dispatch<T>(this T _eventFlags) where T : Enum
        {
            CEventBus<T>.Instance.DispatchEvent(_eventFlags);
        }

        //public static void DispatchDelayed<T>(this T _eventFlags, int _updatesDelay) where T : Enum
        //{
        //    CTimerManager.Instance.RemoveTimerByIdentifier(_eventFlags.ToString());
        //    CTimerManager.Instance.AddWaitForUpdates(_eventFlags.ToString(), _updatesDelay, _t =>
        //    {
        //        CEventBus<T>.Instance.DispatchEvent(_eventFlags);
        //    });
        //}

        public static void Dispatch<T>(this T _eventFlags, CEventBusParams _params) where T : Enum
        {
            CEventBus<T>.Instance.DispatchEvent(_eventFlags, _params);
        }

        public static void Dispatch<T>(this T _eventFlags, int _value) where T : Enum
        {
            CEventBus<T>.Instance.DispatchEvent(_eventFlags, new CEventBusParams { intValue = _value });
        }

        public static void Dispatch<T>(this T _eventFlags, string _value) where T : Enum
        {
            CEventBus<T>.Instance.DispatchEvent(_eventFlags, new CEventBusParams { stringValue = _value });
        }

        public static bool IsSet<T>(this T _eventFlags, T _singleValue) where T : Enum
        {
            return _eventFlags.HasFlag(_singleValue);
        }

        public static void RegisterToEventBus<T>(this IEventBusListener<T> _listener) where T : Enum
        {
            CEventBus<T>.Instance.RegisterListener(_listener);
        }

        public static void UnregisterFromEventBus<T>(this IEventBusListener<T> _listener) where T : Enum
        {
            CEventBus<T>.Instance.UnregisterListener(_listener);
        }

        //public static void NotifyDirty<T>(this IEventBusListener<T> _listener) where T : Enum
        //{
        //    _listener.OnIngameEvent(INGAME_EVENT.DIRTY, null);
        //}
    }

    public interface IEventBusListener<T> where T : Enum
    {
        T GetEventsOfInterest();
        void OnBusEvent(T _event, CEventBusParams _params);
    }

    public class CEventBusParams
    {
        public int intValue;
        public string stringValue;
    }

    public class CEventBus<T> where T : Enum
    {
        private class TempEventBusListener : IEventBusListener<T>
        {
            private T m_event;
            private Action<CEventBusParams> m_callBack;
            private bool m_once;

            public TempEventBusListener(T _event, Action<CEventBusParams> _callBack, bool _once)
            {
                m_event = _event;
                m_callBack = _callBack;
                m_once = _once;
            }

            public T GetEventsOfInterest()
            {
                return m_event;
            }

            public void OnBusEvent(T _event, CEventBusParams _params)
            {
                // prevent initialize self event
                //if ((_event & m_event) == 0)
                //    return;

                m_callBack(_params);

                if (m_once)
                    this.UnregisterFromEventBus();
            }
        }

        private List<IEventBusListener<T>> m_listeners = new List<IEventBusListener<T>>(20);

        private List<List<IEventBusListener<T>>> m_processingListPool = new List<List<IEventBusListener<T>>>();

        private static CEventBus<T> m_instance;
        public static CEventBus<T> Instance
        {
            get
            {
                if (m_instance == null)
                    m_instance = new CEventBus<T>();
                return m_instance;
            }
        }

        public void ClearAllListeners()
        {
            m_listeners.Clear();
        }

        public void DispatchEvent(T _eventFlags)
        {
            DispatchEvent(_eventFlags, default);
        }

        public void DispatchEvent(T _eventFlag, CEventBusParams _params)
        {
            var processingList = RequestProcessingList(_eventFlag);

            for (int i = 0; i < processingList.Count; i++)
            {
                var listener = processingList[i];
                if (listener != null && listener.GetEventsOfInterest().HasFlag(_eventFlag))
                    listener.OnBusEvent(_eventFlag, _params);
            }

            RepoolList(processingList);
        }

        public void RegisterListener(IEventBusListener<T> _listener)
        {
            if (m_listeners.Contains(_listener))
                return;

            m_listeners.Add(_listener);
            //_listener.OnIngameEvent(INGAME_EVENT.INITIALIZED_SELF, default);
        }

        public IEventBusListener<T> RegisterListener(T _event, Action<CEventBusParams> _action, bool _once)
        {
            var listener = new TempEventBusListener(_event, _action, _once);
            RegisterListener(listener);
            return listener;
        }

        public void UnregisterListener(IEventBusListener<T> _listener)
        {
            m_listeners.Remove(_listener);
        }

        private void RepoolList(List<IEventBusListener<T>> _listenerList)
        {
            _listenerList.Clear();
            m_processingListPool.Add(_listenerList);
        }

        private List<IEventBusListener<T>> RequestProcessingList(T _eventFlag)
        {
            List<IEventBusListener<T>> resultList;
            if (m_processingListPool.Count > 0)
            {
                resultList = m_processingListPool[0];
                m_processingListPool.RemoveAt(0);
            }
            else
                resultList = new List<IEventBusListener<T>>(m_listeners.Count);

            var listenersCount = m_listeners.Count;
            for (int i = 0; i < listenersCount; i++)
            {
                var listener = m_listeners[i];
                if (listener.GetEventsOfInterest().HasFlag(_eventFlag))
                    resultList.Add(listener);
            }

            return resultList;
        }
    }
}
