﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Game.Tools
{
    [Serializable]
    [CreateAssetMenu(fileName = "QuickReferences", menuName = "Dev Efficiency/QuickReferences")]
    public class CQuickReferenceHolder : ScriptableObject
    {
        [MenuItem("Olympixel/QuickReferences #q")]
        public static void OpenQuickReferences()
        {
            Selection.activeObject = null;

            var path = $"Assets/Data/ScriptableAssets/Local/QuickReferences_{SystemInfo.deviceUniqueIdentifier}.asset";
            var asset = AssetDatabase.LoadMainAssetAtPath(path);
            if(asset == null)
            {
                // create it
                CQuickReferenceHolder newAsset = ScriptableObject.CreateInstance<CQuickReferenceHolder>();

                AssetDatabase.CreateAsset(newAsset, path);
                AssetDatabase.SaveAssets();
                asset = newAsset;
                Debug.Log($"Created asset: <color=#00FF00>{path}</color>");
            }

            Selection.activeObject = asset;
        }

        [Serializable]
        public struct SReferenceDef
        {
            public string m_name;
            public Object[] m_references;
        }

        public Object[] m_references;
        public SReferenceDef[] m_referenceGroups;
    }
}